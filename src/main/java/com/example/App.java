package com.example;

import jakarta.json.JsonReader;
import org.leadpony.justify.api.JsonSchema;
import org.leadpony.justify.api.JsonValidationService;
import org.leadpony.justify.api.Problem;
import org.leadpony.justify.api.ProblemHandler;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class App {

    public static void main(String[] args) {
        new App().run();
    }

    private JsonValidationService service = JsonValidationService.newInstance();

    private void run() {
        InputStream schemaIn = this.getClass().getResourceAsStream("/examples/example-1/schema.json");
        InputStream dataIn = this.getClass().getResourceAsStream("/examples/example-1/data.json");

        JsonSchema schema = service.readSchema(schemaIn);
        List<Problem> problems = new ArrayList<>();
        ProblemHandler problemHandler = ProblemHandler.collectingTo(problems);

        try (JsonReader reader = service.createReader(dataIn, schema, problemHandler)) {
            reader.readValue();

            problems.forEach(System.out::println);
        }
    }
}
